/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.cm.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.cm.entity.SmCars;
import com.jeesite.modules.cm.service.SmCarsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 车辆管理Controller
 *
 * @author lib
 * @version 2018-04-01
 */
@Controller
@RequestMapping(value = "${adminPath}/cm/smCars")
public class SmCarsController extends BaseController {

    @Autowired
    private SmCarsService smCarsService;

    /**
     * 获取数据
     */
    @ModelAttribute
    public SmCars get(String id, boolean isNewRecord) {
        return smCarsService.get(id, isNewRecord);
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("cm:smCars:view")
    @RequestMapping(value = {"list", ""})
    public String list(SmCars smCars, Model model) {
        model.addAttribute("smCars", smCars);
        return "modules/cm/smCarsList";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("cm:smCars:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<SmCars> listData(SmCars smCars, HttpServletRequest request, HttpServletResponse response) {
        smCars.getSqlMap().getDataScope().addFilter("dsf","Office", "a.office_id","1");
        Page<SmCars> page = smCarsService.findPage(new Page<>(request, response), smCars);
        return page;
    }

    /**
     * 查看编辑表单
     */
    @RequiresPermissions("cm:smCars:view")
    @RequestMapping(value = "form")
    public String form(SmCars smCars, Model model) {
        model.addAttribute("smCars", smCars);
        return "modules/cm/smCarsForm";
    }

    /**
     * 保存车辆管理
     */
    @RequiresPermissions("cm:smCars:edit")
    @PostMapping(value = "save")
    @ResponseBody
    public String save(@Validated SmCars smCars) {
        smCarsService.save(smCars);
        return renderResult(Global.TRUE, "保存车辆管理成功！");
    }

    /**
     * 删除车辆管理
     */
    @RequiresPermissions("cm:smCars:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(SmCars smCars) {
        smCarsService.delete(smCars);
        return renderResult(Global.TRUE, "删除车辆管理成功！");
    }

    /**
     * 车辆列表选择
     */
    @RequestMapping(value = {"carSelect", ""})
    public String carSlect(SmCars smCars, Model model) {
        model.addAttribute("smCars", smCars);
        return "modules/cm/carSelect";
    }
}