/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.cm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.sys.entity.EmpUser;
import com.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 车辆管理Entity
 *
 * @author lib
 * @version 2018-04-01
 */
@Table(name = "sm_cars", alias = "a", columns = {
        @Column(name = "id", attrName = "id", label = "id", isPK = true),
        @Column(name = "car_no", attrName = "carNo", label = "车牌号"),
        @Column(name = "car_brand", attrName = "carBrand", label = "品牌"),
        @Column(name = "car_type", attrName = "carType", label = "车型"),
        @Column(name = "buy_date", attrName = "buyDate", label = "购买日期"),
        @Column(name = "run_date", attrName = "runDate", label = "投运日期"),
        @Column(name = "office_id", attrName = "officeId", label = "所属驾校"),
        @Column(name = "duty_people", attrName = "dutyPeople", label = "负责人", queryType = QueryType.LIKE),
        @Column(name = "park_address", attrName = "parkAddress", label = "常驻地址"),
        @Column(name = "telephone", attrName = "telephone", label = "联系电话"),
        @Column(name = "car_status", attrName = "carStatus", label = "车辆状态"),
        @Column(name = "remarks", attrName = "remarks", label = "备注"),
        @Column(name = "update_time", attrName = "updateTime", label = "更新时间")
}, joinTable = {
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = EmpUser.class, attrName = "empUser", alias = "u12",
                on = "u12.user_code = a.duty_people", columns = {
                @Column(name = "user_code", label = "员工编码", isPK = true),
                @Column(name = "user_name", label = "员工姓名", isQuery = false),
        }),
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = Office.class, attrName = "school", alias = "u13",
                on = "u13.office_code = a.office_id", columns = {
                @Column(name = "office_code", label = "部门编码", isPK = true),
                @Column(name = "office_name", label = "部门名称", isQuery = false),
        })
}, extWhereKeys = "dsf", orderBy = "a.id DESC"
)
public class SmCars extends DataEntity<SmCars> {

    private static final long serialVersionUID = 1L;
    private String carNo;        // 车牌号
    private String carBrand;        // 品牌
    private String carType;        // 车型
    private Date buyDate;        // 购买日期
    private Date runDate;        // 投运日期
    private String officeId;        // 所属驾校
    private String dutyPeople;        // 负责人
    private String parkAddress;        // 常驻地址
    private String telephone;        // 联系电话
    private String carStatus;        // 车辆状态
    private Date updateTime;        // 更新时间
    private EmpUser empUser;        // 负责人
    private Office school;        // 驾校

    public SmCars() {
        this(null);
    }

    public SmCars(String id) {
        super(id);
    }

    @NotBlank(message = "车牌号不能为空")
    @Length(min = 0, max = 255, message = "车牌号长度不能超过 255 个字符")
    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    @NotBlank(message = "品牌不能为空")
    @Length(min = 0, max = 255, message = "品牌长度不能超过 255 个字符")
    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    @NotBlank(message = "车型不能为空")
    @Length(min = 0, max = 255, message = "车型长度不能超过 255 个字符")
    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "投运日期不能为空")
    public Date getRunDate() {
        return runDate;
    }

    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    @NotBlank(message = "所属驾校不能为空")
    @Length(min = 0, max = 64, message = "所属驾校长度不能超过 64 个字符")
    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    @NotBlank(message = "负责人不能为空")
    @Length(min = 0, max = 64, message = "负责人长度不能超过 64 个字符")
    public String getDutyPeople() {
        return dutyPeople;
    }

    public void setDutyPeople(String dutyPeople) {
        this.dutyPeople = dutyPeople;
    }

    @Length(min = 0, max = 255, message = "常驻地址长度不能超过 255 个字符")
    public String getParkAddress() {
        return parkAddress;
    }

    public void setParkAddress(String parkAddress) {
        this.parkAddress = parkAddress;
    }

    @NotBlank(message = "联系电话不能为空")
    @Length(min = 0, max = 50, message = "联系电话长度不能超过 50 个字符")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @NotBlank(message = "车辆状态不能为空")
    @Length(min = 0, max = 2, message = "车辆状态长度不能超过 2 个字符")
    public String getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(String carStatus) {
        this.carStatus = carStatus;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Office getSchool() {
        return school;
    }

    public void setSchool(Office school) {
        this.school = school;
    }

    public EmpUser getEmpUser() {
        return empUser;
    }

    public void setEmpUser(EmpUser empUser) {
        this.empUser = empUser;
    }
}