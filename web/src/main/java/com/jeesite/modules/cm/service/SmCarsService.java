/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.cm.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.cm.dao.SmCarsDao;
import com.jeesite.modules.cm.entity.SmCars;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 车辆管理Service
 * @author lib
 * @version 2018-04-01
 */
@Service
@Transactional(readOnly=true)
public class SmCarsService extends CrudService<SmCarsDao, SmCars> {
	
	/**
	 * 获取单条数据
	 * @param smCars
	 * @return
	 */
	@Override
	public SmCars get(SmCars smCars) {
		return super.get(smCars);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param smCars
	 * @return
	 */
	@Override
	public Page<SmCars> findPage(Page<SmCars> page, SmCars smCars) {
		return super.findPage(page, smCars);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param smCars
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(SmCars smCars) {
		super.save(smCars);
	}
	
	/**
	 * 更新状态
	 * @param smCars
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(SmCars smCars) {
		super.updateStatus(smCars);
	}
	
	/**
	 * 删除数据
	 * @param smCars
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(SmCars smCars) {
		super.delete(smCars);
	}
	
}