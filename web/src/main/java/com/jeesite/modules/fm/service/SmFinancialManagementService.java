/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.fm.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.fm.dao.SmFinancialManagementDao;
import com.jeesite.modules.fm.entity.SmFinancialManagement;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 财务管理Service
 * @author lib
 * @version 2018-04-01
 */
@Service
@Transactional(readOnly=true)
public class SmFinancialManagementService extends CrudService<SmFinancialManagementDao, SmFinancialManagement> {
	
	/**
	 * 获取单条数据
	 * @param smFinancialManagement
	 * @return
	 */
	@Override
	public SmFinancialManagement get(SmFinancialManagement smFinancialManagement) {
		return super.get(smFinancialManagement);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param smFinancialManagement
	 * @return
	 */
	@Override
	public Page<SmFinancialManagement> findPage(Page<SmFinancialManagement> page, SmFinancialManagement smFinancialManagement) {
		return super.findPage(page, smFinancialManagement);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param smFinancialManagement
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(SmFinancialManagement smFinancialManagement) {
		super.save(smFinancialManagement);
	}
	
	/**
	 * 更新状态
	 * @param smFinancialManagement
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(SmFinancialManagement smFinancialManagement) {
		super.updateStatus(smFinancialManagement);
	}
	
	/**
	 * 删除数据
	 * @param smFinancialManagement
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(SmFinancialManagement smFinancialManagement) {
		super.delete(smFinancialManagement);
	}
	
}