/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.fm.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.fm.entity.SmFinancialManagement;
import com.jeesite.modules.fm.service.SmFinancialManagementService;
import com.jeesite.modules.sys.entity.SmSequence;
import com.jeesite.modules.sys.service.SmSequenceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 财务管理Controller
 *
 * @author lib
 * @version 2018-04-01
 */
@Controller
@RequestMapping(value = "${adminPath}/fm/smFinancialManagement")
public class SmFinancialManagementController extends BaseController {

    @Autowired
    private SmFinancialManagementService smFinancialManagementService;

    @Autowired
    private SmSequenceService smSequenceService;

    /**
     * 获取数据
     */
    @ModelAttribute
    public SmFinancialManagement get(String id, boolean isNewRecord) {
        return smFinancialManagementService.get(id, isNewRecord);
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("fm:smFinancialManagement:view")
    @RequestMapping(value = {"list", ""})
    public String list(SmFinancialManagement smFinancialManagement, Model model) {
        model.addAttribute("smFinancialManagement", smFinancialManagement);
        return "modules/fm/smFinancialManagementList";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("fm:smFinancialManagement:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<SmFinancialManagement> listData(SmFinancialManagement smFinancialManagement, HttpServletRequest request, HttpServletResponse response) {
        smFinancialManagement.getSqlMap().getDataScope().addFilter("dsf", "Office", "a.office_id", "1");
        Page<SmFinancialManagement> page = smFinancialManagementService.findPage(new Page<>(request, response), smFinancialManagement);
        return page;
    }

    /**
     * 查看编辑表单
     */
    @RequiresPermissions("fm:smFinancialManagement:view")
    @RequestMapping(value = "form")
    public String form(SmFinancialManagement smFinancialManagement, Model model) {
        if (smFinancialManagement.getIsNewRecord()) {
            SmSequence smSequence = new SmSequence("sm_financial_management", "serial_num");
            smSequence.setPrefix("$");
            smSequence.setDateFormat("yyyyMMdd");
            String serialNum = smSequenceService.getNext(smSequence).getSequenceValue();
            smFinancialManagement.setSerialNum(serialNum);
        }
        model.addAttribute("smFinancialManagement", smFinancialManagement);
        return "modules/fm/smFinancialManagementForm";
    }

    /**
     * 保存财务管理
     */
    @RequiresPermissions("fm:smFinancialManagement:edit")
    @PostMapping(value = "save")
    @ResponseBody
    public String save(@Validated SmFinancialManagement smFinancialManagement) {
        smFinancialManagementService.save(smFinancialManagement);
        return renderResult(Global.TRUE, "保存财务管理成功！");
    }

    /**
     * 删除财务管理
     */
    @RequiresPermissions("fm:smFinancialManagement:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(SmFinancialManagement smFinancialManagement) {
        smFinancialManagementService.delete(smFinancialManagement);
        return renderResult(Global.TRUE, "删除财务管理成功！");
    }

}