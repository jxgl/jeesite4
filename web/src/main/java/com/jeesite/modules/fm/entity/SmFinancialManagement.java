/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.fm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.sys.entity.EmpUser;
import com.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 财务管理Entity
 *
 * @author lib
 * @version 2018-04-01
 */
@Table(name = "sm_financial_management", alias = "a", columns = {
        @Column(name = "id", attrName = "id", label = "id", isPK = true),
        @Column(name = "serial_num", attrName = "serialNum", label = "流水号"),
        @Column(name = "office_id", attrName = "officeId", label = "所属驾校"),
        @Column(name = "pay_type", attrName = "payType", label = "收支类型"),
        @Column(name = "cost_type", attrName = "costType", label = "费用分类"),
        @Column(name = "payment_time", attrName = "paymentTime", label = "发生时间"),
        @Column(name = "transaction_amount", attrName = "transactionAmount", label = "发生金额"),
        @Column(name = "cost_remark", attrName = "costRemark", label = "费用说明"),
        @Column(name = "employee_id", attrName = "employeeId", label = "经办人", queryType = QueryType.LIKE),
        @Column(name = "update_time", attrName = "updateTime", label = "更新时间"),
}, joinTable = {
        @JoinTable(type = Type.LEFT_JOIN, entity = Office.class, alias = "o",
                on = "o.office_code=a.office_id", attrName = "office", columns = {
                @Column(name = "office_code", label = "机构编码", isPK = true),
                @Column(name = "office_name", label = "机构名称", isQuery = false)
        }),
        @JoinTable(type = Type.LEFT_JOIN, entity = EmpUser.class, attrName = "empUser", alias = "u12",
                on = "u12.user_code = a.employee_id", columns = {
                @Column(name = "user_code", label = "员工编码", isPK = true),
                @Column(name = "user_name", label = "员工姓名", isQuery = false),
        })
}, extWhereKeys = "dsf", orderBy = "a.id DESC"
)
public class SmFinancialManagement extends DataEntity<SmFinancialManagement> {

    private static final long serialVersionUID = 1L;
    private String serialNum;        // 流水号
    private String payType;        // 收支类型
    private String officeId;        // 所属驾校
    private String costType;        // 费用分类
    private Date paymentTime;        // 发生时间
    private Double transactionAmount;        // 发生金额
    private String costRemark;        // 费用说明
    private String employeeId;        // 经办人
    private Date updateTime;        // 更新时间
    private Office office;           //经办单位
    private EmpUser empUser;        // 经办人

    public SmFinancialManagement() {
        this(null);
    }

    public SmFinancialManagement(String id) {
        super(id);
    }

    @NotBlank(message = "流水号不能为空")
    @Length(min = 0, max = 50, message = "流水号长度不能超过 50 个字符")
    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    @NotBlank(message = "收支类型不能为空")
    @Length(min = 0, max = 2, message = "收支类型长度不能超过 2 个字符")
    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    @NotBlank(message = "所属驾校不能为空")
    @Length(min = 0, max = 64, message = "所属驾校长度不能超过 64 个字符")
    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    @NotBlank(message = "费用分类不能为空")
    @Length(min = 0, max = 2, message = "费用分类长度不能超过 2 个字符")
    public String getCostType() {
        return costType;
    }

    public void setCostType(String costType) {
        this.costType = costType;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "发生时间不能为空")
    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    @NotNull(message = "发生金额不能为空")
    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    @Length(min = 0, max = 255, message = "费用说明长度不能超过 255 个字符")
    public String getCostRemark() {
        return costRemark;
    }

    public void setCostRemark(String costRemark) {
        this.costRemark = costRemark;
    }

    @NotBlank(message = "经办人不能为空")
    @Length(min = 0, max = 64, message = "经办人长度不能超过 64 个字符")
    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public EmpUser getEmpUser() {
        return empUser;
    }

    public void setEmpUser(EmpUser empUser) {
        this.empUser = empUser;
    }
}