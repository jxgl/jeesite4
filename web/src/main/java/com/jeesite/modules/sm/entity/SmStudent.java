/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.cm.entity.SmCars;
import com.jeesite.modules.dtm.entity.SmDriveTeacher;
import com.jeesite.modules.sys.entity.EmpUser;
import com.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 学员信息表Entity
 *
 * @author lib
 * @version 2018-04-01
 */
@Table(name = "sm_student", alias = "a", columns = {
        @Column(name = "id", attrName = "id", label = "主键", isPK = true),
        @Column(name = "stuno", attrName = "stuno", label = "学员编号"),
        @Column(name = "study_type", attrName = "studyType", label = "报考类型"),
        @Column(name = "card_type", attrName = "cardType", label = "证件类型"),
        @Column(name = "idno", attrName = "idno", label = "证件号"),
        @Column(name = "name", attrName = "name", label = "姓名", queryType = QueryType.LIKE),
        @Column(name = "sex", attrName = "sex", label = "性别"),
        @Column(name = "birth_date", attrName = "birthDate", label = "出生日期"),
        @Column(name = "native_place", attrName = "nativePlace", label = "籍贯"),
        @Column(name = "reg_place", attrName = "regPlace", label = "户口所在地"),
        @Column(name = "address", attrName = "address", label = "住址"),
        @Column(name = "telephone", attrName = "telephone", label = "联系电话"),
        @Column(name = "is_school", attrName = "isSchool", label = "是否本校"),
        @Column(name = "office_id", attrName = "officeId", label = "所属驾校"),
        @Column(name = "teacher", attrName = "teacher", label = "教练员"),
        @Column(name = "use_car", attrName = "useCar", label = "教练车"),
        @Column(name = "study_time", attrName = "studyTime", label = "学车时间"),
        @Column(name = "signup_time", attrName = "signupTime", label = "报名时间"),
        @Column(name = "employee_id", attrName = "employeeId", label = "受理人"),
        @Column(name = "fee", attrName = "fee", label = "学费"),
        @Column(name = "pay_fee", attrName = "payFee", label = "已缴学费"),
        @Column(name = "owe_fee", attrName = "oweFee", label = "剩余学费"),
        @Column(name = "pay_info", attrName = "payInfo", label = "缴费情况"),
        @Column(name = "is_vip", attrName = "isVip", label = "VIP"),
        @Column(name = "remarks", attrName = "remarks", label = "备注"),
        @Column(name = "update_time", attrName = "updateTime", label = "更新时间")
}, joinTable = {
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = EmpUser.class, attrName = "acceptor", alias = "u12",
                on = "u12.user_code = a.employee_id", columns = {
                @Column(name = "user_code", label = "员工编码", isPK = true),
                @Column(name = "user_name", label = "员工姓名", isQuery = false),
        }),
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = Office.class, attrName = "school", alias = "u13",
                on = "u13.office_code = a.office_id", columns = {
                @Column(name = "office_code", label = "部门编码", isPK = true),
                @Column(name = "office_name", label = "部门名称", isQuery = false),
        }),
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = SmCars.class, attrName = "car", alias = "c1",
                on = "c1.id = a.use_car", columns = {
                @Column(name = "id", label = "car主键", isPK = true),
                @Column(name = "car_no", label = "车牌号", isQuery = false),
        }),
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = SmDriveTeacher.class, attrName = "driveTeacher", alias = "t1",
                on = "t1.id = a.teacher", columns = {
                @Column(name = "id", label = "教练id", isPK = true),
                @Column(name = "name", label = "教练名称", isQuery = false),
        })
}, extWhereKeys = "dsf",orderBy = "a.id DESC"
)
public class SmStudent extends DataEntity<SmStudent> {

    private static final long serialVersionUID = 1L;
    /**
     * 证件号
     */
    private String idno;

    /**
     * 学员编号
     */
    private String stuno;

    /**
     * 姓名
     */
    private String name;

    /**
     * 证件类型
     */
    private String cardType;

    /**
     * 性别
     */
    private String sex;

    /**
     * 出生日期
     */
    private Date birthDate;

    /**
     * 籍贯
     */
    private String nativePlace;

    /**
     * 户口所在地
     */
    private String regPlace;

    /**
     * 住址
     */
    private String address;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 是否本校
     */
    private String isSchool;

    /**
     * 所属驾校
     */
    private String officeId;

    /**
     * 报名时间
     */
    private Date signupTime;

    /**
     * 受理人
     */
    private String employeeId;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 教练员
     */
    private String teacher;

    /**
     * 教练车
     */
    private String useCar;

    /**
     * 学车时间
     */
    private Date studyTime;

    /**
     * 学费
     */
    private Integer fee;

    /**
     * 已缴学费
     */
    private Integer payFee;

    /**
     * 剩余学费
     */
    private Integer oweFee;

    /**
     * 缴费情况
     */
    private String payInfo;

    /**
     * VIP
     */
    private String isVip;

    /**
     * 报考类型
     */
    private String studyType;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 受理人
     */
    private EmpUser acceptor;

    /**
     * 驾校
     */
    private Office school;

    /**
     * 车辆
     */
    private SmCars car;

    /**
     * 教练
     */
    private SmDriveTeacher driveTeacher;

    public SmStudent() {
        this(null);
    }

    public SmStudent(String id) {
        super(id);
    }

    @NotBlank(message = "证件号不能为空")
    @Length(min = 0, max = 50, message = "证件号长度不能超过 50 个字符")
    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    @NotBlank(message = "学员编号不能为空")
    @Length(min = 0, max = 16, message = "学员编号长度不能超过 16 个字符")
    public String getStuno() {
        return stuno;
    }

    public void setStuno(String stuno) {
        this.stuno = stuno;
    }

    @NotBlank(message = "姓名不能为空")
    @Length(min = 0, max = 100, message = "姓名长度不能超过 100 个字符")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotBlank(message = "证件类型不能为空")
    @Length(min = 0, max = 2, message = "证件类型长度不能超过 2 个字符")
    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @NotBlank(message = "性别不能为空")
    @Length(min = 0, max = 2, message = "性别长度不能超过 2 个字符")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Length(min = 0, max = 255, message = "籍贯长度不能超过 255 个字符")
    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    @Length(min = 0, max = 255, message = "户口所在地长度不能超过 255 个字符")
    public String getRegPlace() {
        return regPlace;
    }

    public void setRegPlace(String regPlace) {
        this.regPlace = regPlace;
    }

    @Length(min = 0, max = 255, message = "住址长度不能超过 255 个字符")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @NotBlank(message = "联系电话不能为空")
    @Length(min = 0, max = 20, message = "联系电话长度不能超过 20 个字符")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @NotBlank(message = "是否本校不能为空")
    @Length(min = 0, max = 20, message = "是否本校长度不能超过 2 个字符")
    public String getIsSchool() {
        return isSchool;
    }

    public void setIsSchool(String isSchool) {
        this.isSchool = isSchool;
    }

    @NotBlank(message = "所属驾校不能为空")
    @Length(min = 0, max = 64, message = "所属驾校长度不能超过 64 个字符")
    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getSignupTime() {
        return signupTime;
    }

    public void setSignupTime(Date signupTime) {
        this.signupTime = signupTime;
    }

    @NotBlank(message = "受理人不能为空")
    @Length(min = 0, max = 64, message = "受理人长度不能超过 64 个字符")
    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Length(min = 0, max = 64, message = "教练员长度不能超过 64 个字符")
    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    @Length(min = 0, max = 64, message = "教练车长度不能超过 64 个字符")
    public String getUseCar() {
        return useCar;
    }

    public void setUseCar(String useCar) {
        this.useCar = useCar;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public Date getStudyTime() {
        return studyTime;
    }

    public void setStudyTime(Date studyTime) {
        this.studyTime = studyTime;
    }

    @NotNull(message = "收费标准不能为空")
    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    @NotNull(message = "应缴学费不能为空")
    public Integer getPayFee() {
        return payFee;
    }

    public void setPayFee(Integer payFee) {
        this.payFee = payFee;
    }

    @NotNull(message = "剩余学费不能为空")
    public Integer getOweFee() {
        return oweFee;
    }

    public void setOweFee(Integer oweFee) {
        this.oweFee = oweFee;
    }

    @NotBlank(message = "缴费情况不能为空")
    @Length(min = 0, max = 2, message = "缴费情况长度不能超过 2 个字符")
    public String getPayInfo() {
        return payInfo;
    }

    public void setPayInfo(String payInfo) {
        this.payInfo = payInfo;
    }

    @NotBlank(message = "VIP不能为空")
    @Length(min = 0, max = 2, message = "VIP长度不能超过 2 个字符")
    public String getIsVip() {
        return isVip;
    }

    public void setIsVip(String isVip) {
        this.isVip = isVip;
    }

    @NotBlank(message = "报考类型不能为空")
    @Length(min = 0, max = 2, message = "报考类型长度不能超过 2 个字符")
    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    @Override
    public String getRemarks() {
        return remarks;
    }

    @Override
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public EmpUser getAcceptor() {
        return acceptor;
    }

    public void setAcceptor(EmpUser acceptor) {
        this.acceptor = acceptor;
    }

    public Office getSchool() {
        return school;
    }

    public void setSchool(Office school) {
        this.school = school;
    }

    public SmCars getCar() {
        return car;
    }

    public void setCar(SmCars car) {
        this.car = car;
    }

    public SmDriveTeacher getDriveTeacher() {
        return driveTeacher;
    }

    public void setDriveTeacher(SmDriveTeacher driveTeacher) {
        this.driveTeacher = driveTeacher;
    }
}