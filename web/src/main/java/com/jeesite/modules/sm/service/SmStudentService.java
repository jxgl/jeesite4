/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sm.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.sm.dao.SmStudentDao;
import com.jeesite.modules.sm.entity.SmStudent;
import com.jeesite.modules.sys.service.SmAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 学员信息表Service
 *
 * @author lib
 * @version 2018-04-01
 */
@Service
@Transactional(readOnly = true)
public class SmStudentService extends CrudService<SmStudentDao, SmStudent> {

    @Autowired
    private SmAuditService smAuditService;

    /**
     * 获取单条数据
     *
     * @param smStudent
     * @return
     */
    @Override
    public SmStudent get(SmStudent smStudent) {
        return super.get(smStudent);
    }

    /**
     * 查询分页数据
     *
     * @param page      分页对象
     * @param smStudent
     * @return
     */
    @Override
    public Page<SmStudent> findPage(Page<SmStudent> page, SmStudent smStudent) {
        return super.findPage(page, smStudent);
    }

    /**
     * 保存数据（插入或更新）
     *
     * @param smStudent
     */
    @Override
    @Transactional(readOnly = false)
    public void save(SmStudent smStudent) {
        super.save(smStudent);
    }

    /**
     * 更新状态
     *
     * @param smStudent
     */
    @Override
    @Transactional(readOnly = false)
    public void updateStatus(SmStudent smStudent) {
        super.updateStatus(smStudent);
    }

    /**
     * 删除数据
     *
     * @param smStudent
     */
    @Override
    @Transactional(readOnly = false)
    public void delete(SmStudent smStudent) {
        super.delete(smStudent);
    }

}