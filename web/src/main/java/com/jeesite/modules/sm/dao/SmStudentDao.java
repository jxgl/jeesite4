/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sm.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.sm.entity.SmStudent;

/**
 * 学员信息表DAO接口
 * @author lib
 * @version 2018-04-01
 */
@MyBatisDao
public interface SmStudentDao extends CrudDao<SmStudent> {
	
}