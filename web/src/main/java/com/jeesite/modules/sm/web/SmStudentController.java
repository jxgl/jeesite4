/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sm.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.sm.entity.SmStudent;
import com.jeesite.modules.sm.service.SmStudentService;
import com.jeesite.modules.sys.entity.SmSequence;
import com.jeesite.modules.sys.service.SmSequenceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 学员信息表Controller
 *
 * @author lib
 * @version 2018-04-01
 */
@Controller
@RequestMapping(value = "${adminPath}/sm/smStudent")
public class SmStudentController extends BaseController {

    @Autowired
    private SmStudentService smStudentService;

    @Autowired
    private SmSequenceService smSequenceService;

    /**
     * 获取数据
     */
    @ModelAttribute
    public SmStudent get(String id, boolean isNewRecord) {
        return smStudentService.get(id, isNewRecord);
    }

    /**
     * 根据主键获取单个学生信息
     *
     * @param smStudent 主键
     * @return
     */
    @GetMapping("get")
    @ResponseBody
    public SmStudent getById(SmStudent smStudent) {
        return smStudent;
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("sm:smStudent:view")
    @RequestMapping(value = {"list", ""})
    public String list(SmStudent smStudent, Model model) {
        model.addAttribute("smStudent", smStudent);
        return "modules/sm/smStudentList";
    }

    @RequestMapping(value = {"studentSelect", ""})
    public String studentSelect(SmStudent smStudent, Model model) {
        model.addAttribute("smStudent", smStudent);
        return "modules/sm/studentSelect";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("sm:smStudent:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<SmStudent> listData(SmStudent smStudent, HttpServletRequest request, HttpServletResponse response) {
        smStudent.getSqlMap().getDataScope().addFilter("dsf", "Office", "a.office_id", "1");
        Page<SmStudent> page = smStudentService.findPage(new Page<>(request, response), smStudent);
        return page;
    }

    /**
     * 查看编辑表单
     */
    @RequiresPermissions("sm:smStudent:view")
    @RequestMapping(value = "form")
    public String form(SmStudent smStudent, Model model) {
        if (smStudent.getIsNewRecord()) {
            SmSequence smSequence = new SmSequence("sm_student", "stuno");
            smSequence.setPrefix("S");
            smStudent.setStuno(smSequenceService.getNext(smSequence).getSequenceValue());
            smStudent.setSignupTime(new Date());
        }
        model.addAttribute("smStudent", smStudent);
        return "modules/sm/smStudentForm";
    }

    /**
     * 保存学员信息表
     */
    @RequiresPermissions("sm:smStudent:edit")
    @PostMapping(value = "save")
    @ResponseBody
    public String save(@Validated SmStudent smStudent) {
        smStudentService.save(smStudent);
        return renderResult(Global.TRUE, "保存学员信息表成功！");
    }

    /**
     * 删除学员信息表
     */
    @RequiresPermissions("sm:smStudent:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(SmStudent smStudent) {
        smStudentService.delete(smStudent);
        return renderResult(Global.TRUE, "删除学员信息表成功！");
    }

}