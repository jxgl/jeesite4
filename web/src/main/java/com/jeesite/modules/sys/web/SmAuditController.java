/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.sys.entity.SmAudit;
import com.jeesite.modules.sys.service.SmAuditService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 审计表Controller
 * @author lib
 * @version 2018-04-13
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/smAudit")
public class SmAuditController extends BaseController {

	@Autowired
	private SmAuditService smAuditService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public SmAudit get(String id, boolean isNewRecord) {
		return smAuditService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("sys:smAudit:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmAudit smAudit, Model model) {
		model.addAttribute("smAudit", smAudit);
		return "modules/sys/smAuditList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("sys:smAudit:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<SmAudit> listData(SmAudit smAudit, HttpServletRequest request, HttpServletResponse response) {
		Page<SmAudit> page = smAuditService.findPage(new Page<SmAudit>(request, response), smAudit); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("sys:smAudit:view")
	@RequestMapping(value = "form")
	public String form(SmAudit smAudit, Model model) {
		model.addAttribute("smAudit", smAudit);
		return "modules/sys/smAuditForm";
	}

	/**
	 * 保存审计表
	 */
	@RequiresPermissions("sys:smAudit:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated SmAudit smAudit) {
		smAuditService.save(smAudit);
		return renderResult(Global.TRUE, "保存审计表成功！");
	}
	
}