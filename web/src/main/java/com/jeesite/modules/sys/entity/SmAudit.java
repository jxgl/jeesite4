/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 审计表Entity
 * @author lib
 * @version 2018-04-13
 */
@Table(name="sm_audit", alias="a", columns={
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(name="os_type", attrName="osType", label="业务类型", comment="业务类型（学员报名，财务）"),
		@Column(name="op_user_name", attrName="opUserName", label="操作用户名称", queryType=QueryType.LIKE),
		@Column(name="op_user_id", attrName="opUserId", label="操作用户id"),
		@Column(name="ip_addr", attrName="ipAddr", label="客户端ip"),
		@Column(name="op_content", attrName="opContent", label="操作内容"),
		@Column(name="op_time", attrName="opTime", label="操作时间"),
	}, orderBy="a.id DESC"
)
public class SmAudit extends DataEntity<SmAudit> {
	
	private static final long serialVersionUID = 1L;
	private String osType;		// 业务类型（学员报名，财务）
	private String opUserName;		// 操作用户名称
	private String opUserId;		// 操作用户id
	private String ipAddr;		// 客户端ip
	private String opContent;		// 操作内容
	private Date opTime;		// 操作时间
	
	public SmAudit() {
		this(null);
	}

	public SmAudit(String id){
		super(id);
	}
	
	@NotBlank(message="业务类型不能为空")
	@Length(min=0, max=2, message="业务类型长度不能超过 2 个字符")
	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}
	
	@NotBlank(message="操作用户名称不能为空")
	@Length(min=0, max=100, message="操作用户名称长度不能超过 100 个字符")
	public String getOpUserName() {
		return opUserName;
	}

	public void setOpUserName(String opUserName) {
		this.opUserName = opUserName;
	}
	
	@NotBlank(message="操作用户id不能为空")
	@Length(min=0, max=64, message="操作用户id长度不能超过 64 个字符")
	public String getOpUserId() {
		return opUserId;
	}

	public void setOpUserId(String opUserId) {
		this.opUserId = opUserId;
	}
	
	@NotBlank(message="客户端ip不能为空")
	@Length(min=0, max=15, message="客户端ip长度不能超过 15 个字符")
	public String getIpAddr() {
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	
	@NotBlank(message="操作内容不能为空")
	@Length(min=0, max=2000, message="操作内容长度不能超过 2000 个字符")
	public String getOpContent() {
		return opContent;
	}

	public void setOpContent(String opContent) {
		this.opContent = opContent;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="操作时间不能为空")
	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}
	
}