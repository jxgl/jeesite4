/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.sys.dao.SmAuditDao;
import com.jeesite.modules.sys.entity.SmAudit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 审计表Service
 * @author lib
 * @version 2018-04-13
 */
@Service
@Transactional(readOnly=true)
public class SmAuditService extends CrudService<SmAuditDao, SmAudit> {
	
	/**
	 * 获取单条数据
	 * @param smAudit
	 * @return
	 */
	@Override
	public SmAudit get(SmAudit smAudit) {
		return super.get(smAudit);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param smAudit
	 * @return
	 */
	@Override
	public Page<SmAudit> findPage(Page<SmAudit> page, SmAudit smAudit) {
		return super.findPage(page, smAudit);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param smAudit
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(SmAudit smAudit) {
		super.save(smAudit);
	}
	
	/**
	 * 更新状态
	 * @param smAudit
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(SmAudit smAudit) {
		super.updateStatus(smAudit);
	}
	
	/**
	 * 删除数据
	 * @param smAudit
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(SmAudit smAudit) {
		super.delete(smAudit);
	}
	
}