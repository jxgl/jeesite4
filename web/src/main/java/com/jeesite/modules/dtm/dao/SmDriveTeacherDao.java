/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.dtm.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.dtm.entity.SmDriveTeacher;

/**
 * 教练DAO接口
 * @author lib
 * @version 2018-04-01
 */
@MyBatisDao
public interface SmDriveTeacherDao extends CrudDao<SmDriveTeacher> {
	
}