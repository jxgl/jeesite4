/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.dtm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 教练Entity
 *
 * @author lib
 * @version 2018-04-01
 */
@Table(name = "sm_drive_teacher", alias = "a", columns = {
        @Column(name = "id", attrName = "id", label = "主键", isPK = true),
        @Column(name = "teacher_no", attrName = "teacherNo", label = "教练编号"),
        @Column(name = "name", attrName = "name", label = "姓名", queryType = QueryType.LIKE),
        @Column(name = "sex", attrName = "sex", label = "性别"),
        @Column(name = "card_type", attrName = "cardType", label = "证件类型"),
        @Column(name = "idno", attrName = "idno", label = "证件号"),
        @Column(name = "birth_date", attrName = "birthDate", label = "出生日期"),
        @Column(name = "native_place", attrName = "nativePlace", label = "籍贯"),
        @Column(name = "reg_place", attrName = "regPlace", label = "户口所在地"),
        @Column(name = "address", attrName = "address", label = "住址"),
        @Column(name = "telephone", attrName = "telephone", label = "联系电话"),
        @Column(name = "office_id", attrName = "officeId", label = "所属驾校"),
        @Column(name = "school_time", attrName = "schoolTime", label = "任教时间"),
        @Column(name = "teacher_status", attrName = "teacherStatus", label = "状态"),
        @Column(name = "remarks", attrName = "remarks", label = "备注"),
        @Column(name = "update_time", attrName = "updateTime", label = "更新时间")
}, joinTable = {
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = Office.class, attrName = "school", alias = "u13",
                on = "u13.office_code = a.office_id", columns = {
                @Column(name = "office_code", label = "部门编码", isPK = true),
                @Column(name = "office_name", label = "部门名称", isQuery = false),
        })},extWhereKeys = "dsf", orderBy = "a.id DESC"
)
public class SmDriveTeacher extends DataEntity<SmDriveTeacher> {

    private static final long serialVersionUID = 1L;
    private String teacherNo;        // 教练编号
    private String name;        // 姓名
    private String sex;        // 性别
    private String cardType;        // 证件类型
    private String idno;        // 证件号
    private Date birthDate;        // 出生日期
    private String nativePlace;        // 籍贯
    private String regPlace;        // 户口所在地
    private String address;        // 住址
    private String telephone;        // 联系电话
    private String officeId;        // 所属驾校
    private Date schoolTime;        // 任教时间
    private Date updateTime;        // 更新时间
    private String remarks;        // 备注
    private String teacherStatus;        // 状态
    private Office school;        // 驾校

    public SmDriveTeacher() {
        this(null);
    }

    public SmDriveTeacher(String id) {
        super(id);
    }

    @NotBlank(message = "证件号不能为空")
    @Length(min = 0, max = 50, message = "证件号长度不能超过 50 个字符")
    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    @NotBlank(message = "姓名不能为空")
    @Length(min = 0, max = 50, message = "姓名长度不能超过 50 个字符")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotBlank(message = "性别不能为空")
    @Length(min = 0, max = 2, message = "性别长度不能超过 2 个字符")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Length(min = 0, max = 255, message = "籍贯长度不能超过 255 个字符")
    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    @Length(min = 0, max = 255, message = "户口所在地长度不能超过 255 个字符")
    public String getRegPlace() {
        return regPlace;
    }

    public void setRegPlace(String regPlace) {
        this.regPlace = regPlace;
    }

    @Length(min = 0, max = 255, message = "住址长度不能超过 255 个字符")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @NotBlank(message = "联系电话不能为空")
    @Length(min = 0, max = 255, message = "联系电话长度不能超过 255 个字符")
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @NotBlank(message = "所属驾校不能为空")
    @Length(min = 0, max = 64, message = "所属驾校长度不能超过 64 个字符")
    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "任教时间不能为空")
    public Date getSchoolTime() {
        return schoolTime;
    }

    public void setSchoolTime(Date schoolTime) {
        this.schoolTime = schoolTime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @NotBlank(message = "状态不能为空")
    @Length(min = 0, max = 2, message = "状态长度不能超过 2 个字符")
    public String getTeacherStatus() {
        return teacherStatus;
    }

    public void setTeacherStatus(String teacherStatus) {
        this.teacherStatus = teacherStatus;
    }

    public String getTeacherNo() {
        return teacherNo;
    }

    public void setTeacherNo(String teacherNo) {
        this.teacherNo = teacherNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @Override
    public String getRemarks() {
        return remarks;
    }

    @Override
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Office getSchool() {
        return school;
    }

    public void setSchool(Office school) {
        this.school = school;
    }
}