/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.dtm.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.dtm.dao.SmDriveTeacherDao;
import com.jeesite.modules.dtm.entity.SmDriveTeacher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 教练Service
 * @author lib
 * @version 2018-04-01
 */
@Service
@Transactional(readOnly=true)
public class SmDriveTeacherService extends CrudService<SmDriveTeacherDao, SmDriveTeacher> {
	
	/**
	 * 获取单条数据
	 * @param smDriveTeacher
	 * @return
	 */
	@Override
	public SmDriveTeacher get(SmDriveTeacher smDriveTeacher) {
		return super.get(smDriveTeacher);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param smDriveTeacher
	 * @return
	 */
	@Override
	public Page<SmDriveTeacher> findPage(Page<SmDriveTeacher> page, SmDriveTeacher smDriveTeacher) {
		return super.findPage(page, smDriveTeacher);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param smDriveTeacher
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(SmDriveTeacher smDriveTeacher) {
		super.save(smDriveTeacher);
	}
	
	/**
	 * 更新状态
	 * @param smDriveTeacher
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(SmDriveTeacher smDriveTeacher) {
		super.updateStatus(smDriveTeacher);
	}
	
	/**
	 * 删除数据
	 * @param smDriveTeacher
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(SmDriveTeacher smDriveTeacher) {
		super.delete(smDriveTeacher);
	}
	
}