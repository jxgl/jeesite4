/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.dtm.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.dtm.entity.SmDriveTeacher;
import com.jeesite.modules.dtm.service.SmDriveTeacherService;
import com.jeesite.modules.sys.entity.SmSequence;
import com.jeesite.modules.sys.service.SmSequenceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 教练Controller
 *
 * @author lib
 * @version 2018-04-01
 */
@Controller
@RequestMapping(value = "${adminPath}/dtm/smDriveTeacher")
public class SmDriveTeacherController extends BaseController {

    @Autowired
    private SmDriveTeacherService smDriveTeacherService;

    @Autowired
    private SmSequenceService smSequenceService;

    /**
     * 获取数据
     */
    @ModelAttribute
    public SmDriveTeacher get(String id, boolean isNewRecord) {
        return smDriveTeacherService.get(id, isNewRecord);
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("dtm:smDriveTeacher:view")
    @RequestMapping(value = {"list", ""})
    public String list(SmDriveTeacher smDriveTeacher, Model model) {
        model.addAttribute("smDriveTeacher", smDriveTeacher);
        return "modules/dtm/smDriveTeacherList";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("dtm:smDriveTeacher:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<SmDriveTeacher> listData(SmDriveTeacher smDriveTeacher, HttpServletRequest request, HttpServletResponse response) {
        smDriveTeacher.getSqlMap().getDataScope().addFilter("dsf", "Office", "a.office_id", "1");
        return smDriveTeacherService.findPage(new Page<>(request, response), smDriveTeacher);
    }

    /**
     * 查看编辑表单
     */
    @RequiresPermissions("dtm:smDriveTeacher:view")
    @RequestMapping(value = "form")
    public String form(SmDriveTeacher smDriveTeacher, Model model) {
        if (smDriveTeacher.getIsNewRecord()) {
            SmSequence smSequence = new SmSequence("sm_drive_teacher", "teacher_no");
            smSequence.setPrefix("T");
            SmSequence sequence = smSequenceService.getNext(smSequence);
            smDriveTeacher.setTeacherNo(sequence.getSequenceValue());
        }

        model.addAttribute("smDriveTeacher", smDriveTeacher);
        return "modules/dtm/smDriveTeacherForm";
    }

    /**
     * 保存教练
     */
    @RequiresPermissions("dtm:smDriveTeacher:edit")
    @PostMapping(value = "save")
    @ResponseBody
    public String save(@Validated SmDriveTeacher smDriveTeacher) {
        smDriveTeacher.setUpdateTime(new Date());
        smDriveTeacherService.save(smDriveTeacher);
        return renderResult(Global.TRUE, "保存教练成功！");
    }

    /**
     * 删除教练
     */
    @RequiresPermissions("dtm:smDriveTeacher:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(SmDriveTeacher smDriveTeacher) {
        smDriveTeacherService.delete(smDriveTeacher);
        return renderResult(Global.TRUE, "删除教练成功！");
    }

    /**
     * 教练选择
     */
    @RequestMapping(value = {"teacherSelect", ""})
    public String teacherSelect(SmDriveTeacher smDriveTeacher, Model model) {
        model.addAttribute("smDriveTeacher", smDriveTeacher);
        return "modules/dtm/teacherSelect";
    }
}