/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.tm.web;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.tm.entity.SmTestOrder;
import com.jeesite.modules.tm.service.SmTestOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 考试预约Controller
 *
 * @author lib
 * @version 2018-04-01
 */
@Controller
@RequestMapping(value = "${adminPath}/tm/smTestOrder")
public class SmTestOrderController extends BaseController {

    @Autowired
    private SmTestOrderService smTestOrderService;

    /**
     * 获取数据
     */
    @ModelAttribute
    public SmTestOrder get(String id, boolean isNewRecord) {
        return smTestOrderService.get(id, isNewRecord);
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("tm:smTestOrder:view")
    @RequestMapping(value = {"list", ""})
    public String list(SmTestOrder smTestOrder, Model model) {
        model.addAttribute("smTestOrder", smTestOrder);
        return "modules/tm/smTestOrderList";
    }

    /**
     * 查询列表数据
     */
    @RequiresPermissions("tm:smTestOrder:view")
    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<SmTestOrder> listData(SmTestOrder smTestOrder, HttpServletRequest request, HttpServletResponse response) {
        smTestOrder.getSqlMap().getDataScope().addFilter("dsf", "Office", "a.office_id", "1");
        Page<SmTestOrder> page = smTestOrderService.findPage(new Page<>(request, response), smTestOrder);
        return page;
    }

    /**
     * 查看编辑表单
     */
    @RequiresPermissions("tm:smTestOrder:view")
    @RequestMapping(value = "form")
    public String form(SmTestOrder smTestOrder, Model model) {
        model.addAttribute("smTestOrder", smTestOrder);
        return "modules/tm/smTestOrderForm";
    }

    /**
     * 保存考试预约
     */
    @RequiresPermissions("tm:smTestOrder:edit")
    @PostMapping(value = "save")
    @ResponseBody
    public String save(@Validated SmTestOrder smTestOrder) {
        smTestOrderService.save(smTestOrder);
        return renderResult(Global.TRUE, "保存考试预约成功！");
    }

    /**
     * 删除考试预约
     */
    @RequiresPermissions("tm:smTestOrder:edit")
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(SmTestOrder smTestOrder) {
        smTestOrderService.delete(smTestOrder);
        return renderResult(Global.TRUE, "删除考试预约成功！");
    }

}