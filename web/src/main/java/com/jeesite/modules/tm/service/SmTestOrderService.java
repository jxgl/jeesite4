/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.tm.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.tm.dao.SmTestOrderDao;
import com.jeesite.modules.tm.entity.SmTestOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 考试预约Service
 * @author lib
 * @version 2018-04-01
 */
@Service
@Transactional(readOnly=true)
public class SmTestOrderService extends CrudService<SmTestOrderDao, SmTestOrder> {
	
	/**
	 * 获取单条数据
	 * @param smTestOrder
	 * @return
	 */
	@Override
	public SmTestOrder get(SmTestOrder smTestOrder) {
		return super.get(smTestOrder);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param smTestOrder
	 * @return
	 */
	@Override
	public Page<SmTestOrder> findPage(Page<SmTestOrder> page, SmTestOrder smTestOrder) {
		return super.findPage(page, smTestOrder);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param smTestOrder
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(SmTestOrder smTestOrder) {
		super.save(smTestOrder);
	}
	
	/**
	 * 更新状态
	 * @param smTestOrder
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(SmTestOrder smTestOrder) {
		super.updateStatus(smTestOrder);
	}
	
	/**
	 * 删除数据
	 * @param smTestOrder
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(SmTestOrder smTestOrder) {
		super.delete(smTestOrder);
	}
	
}