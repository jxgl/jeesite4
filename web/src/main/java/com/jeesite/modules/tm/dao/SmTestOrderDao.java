/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.tm.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.tm.entity.SmTestOrder;

/**
 * 考试预约DAO接口
 * @author lib
 * @version 2018-04-01
 */
@MyBatisDao
public interface SmTestOrderDao extends CrudDao<SmTestOrder> {
	
}