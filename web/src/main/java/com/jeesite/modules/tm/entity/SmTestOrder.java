/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.modules.sm.entity.SmStudent;
import com.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 考试预约Entity
 *
 * @author lib
 * @version 2018-04-01
 */
@Table(name = "sm_test_order", alias = "a", columns = {
        @Column(name = "id", attrName = "id", label = "主键", isPK = true),
        @Column(name = "student_id", attrName = "studentId", label = "考生"),
        @Column(name = "subject", attrName = "subject", label = "考试科目"),
        @Column(name = "test_date", attrName = "testDate", label = "考试日期"),
        @Column(name = "test_addr", attrName = "testAddr", label = "考试场地"),
        @Column(name = "test_status", attrName = "testStatus", label = "考试状态"),
        @Column(name = "office_id", attrName = "officeId", label = "所属驾校"),
        @Column(name = "order_date", attrName = "orderDate", label = "预约时间"),
        @Column(name = "remarks", attrName = "remarks", label = "备注"),
        @Column(name = "update_time", attrName = "updateTime", label = "更新时间")
}, joinTable = {
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = SmStudent.class, attrName = "student", alias = "u12",
                on = "u12.id = a.student_id", columns = {
                @Column(name = "id", label = "考生id", isPK = true),
                @Column(name = "name", label = "考生姓名", isQuery = false),
                @Column(name = "sex", label = "性别", isQuery = false),
                @Column(name = "card_type", label = "证件类型", isQuery = false),
                @Column(name = "idno", label = "证件号", isQuery = false),
                @Column(name = "telephone", label = "电话", isQuery = false),
                @Column(name = "study_type", label = "考试类型", isQuery = false),
                @Column(name = "is_vip", label = "是否VIP", isQuery = false),
                @Column(name = "is_school", label = "是否本校", isQuery = false)
        }),
        @JoinTable(type = JoinTable.Type.LEFT_JOIN, entity = Office.class, attrName = "school", alias = "u13",
                on = "u13.office_code = a.office_id", columns = {
                @Column(name = "office_code", label = "部门编码", isPK = true),
                @Column(name = "office_name", label = "部门名称", isQuery = false),
                @Column(name = "leader", label = "负责人", isQuery = false),
                @Column(name = "phone", label = "办公电话", isQuery = false)
        })
}, extWhereKeys = "dsf", orderBy = "a.id DESC"
)
public class SmTestOrder extends DataEntity<SmTestOrder> {

    private static final long serialVersionUID = 1L;
    private String studentId;        // 考生
    private String subject;        // 考试科目
    private Date testDate;        // 考试日期
    private String testAddr;        // 考试场地
    private String testStatus;        // 考试状态
    private String officeId;        // 所属驾校
    private Date orderDate;        // 预约时间
    private String remarks;        // 备注
    private Date updateTime;        // 更新时间
    private Office school; //驾校
    private SmStudent student;//考生

    public SmTestOrder() {
        this(null);
    }

    public SmTestOrder(String id) {
        super(id);
    }

    @NotBlank(message = "考试科目不能为空")
    @Length(min = 0, max = 10, message = "考试科目长度不能超过 2 个字符")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "考试日期不能为空")
    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    @NotBlank(message = "考试场地不能为空")
    @Length(min = 0, max = 255, message = "考试场地长度不能超过 255 个字符")
    public String getTestAddr() {
        return testAddr;
    }

    public void setTestAddr(String testAddr) {
        this.testAddr = testAddr;
    }

    @NotBlank(message = "所属驾校不能为空")
    @Length(min = 0, max = 64, message = "所属驾校长度不能超过 64 个字符")
    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "预约时间不能为空")
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    @Override
    public String getRemarks() {
        return remarks;
    }

    @Override
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Office getSchool() {
        return school;
    }

    public void setSchool(Office school) {
        this.school = school;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public SmStudent getStudent() {
        return student;
    }

    public void setStudent(SmStudent student) {
        this.student = student;
    }
}