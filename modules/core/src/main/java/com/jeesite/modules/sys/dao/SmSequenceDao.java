/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.sys.entity.SmSequence;

/**
 * 编号自增表DAO接口
 * @author lib
 * @version 2018-04-11
 */
@MyBatisDao
public interface SmSequenceDao extends CrudDao<SmSequence> {
	
}