/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.entity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 编号自增表Entity
 *
 * @author lib
 * @version 2018-04-11
 */
@Table(name = "js_sys_sequence", alias = "a", columns = {
        @Column(name = "id", attrName = "id", label = "id", isPK = true),
        @Column(name = "table_name", attrName = "tableName", label = "表名", queryType = QueryType.LIKE),
        @Column(name = "column_name", attrName = "columnName", label = "字段名", queryType = QueryType.LIKE),
        @Column(name = "sequence_value", attrName = "sequenceValue", label = "序列值"),
}, orderBy = "a.id DESC"
)
public class SmSequence extends DataEntity<SmSequence> {

    private static String DEFAULT_FMT = "yyyyMM";
    private static int DEFAULT_SERIALLENGTH = 4;

    private static final long serialVersionUID = 1L;
    /**
     * 表名
     */
    private String tableName;

    /**
     * 字段名
     */
    private String columnName;

    /**
     * 序列值
     */
    private String sequenceValue;

    /**
     * 前缀
     */
    private String prefix;

    /**
     * 日期格式
     */
    private String dateFormat = DEFAULT_FMT;

    /**
     * 序列长度
     */
    private int serialLength = DEFAULT_SERIALLENGTH;

    public SmSequence() {
        this(null);
    }

    public SmSequence(String id) {
        super(id);
    }

    public SmSequence(String tableName, String columnName) {
        this.tableName = tableName;
        this.columnName = columnName;
    }

    @NotBlank(message = "表名不能为空")
    @Length(min = 0, max = 50, message = "表名长度不能超过 50 个字符")
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @NotBlank(message = "字段名不能为空")
    @Length(min = 0, max = 50, message = "字段名长度不能超过 50 个字符")
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @NotBlank(message = "序列值不能为空")
    @Length(min = 0, max = 10, message = "序列值长度不能超过 10 个字符")
    public String getSequenceValue() {
        return sequenceValue;
    }

    public void setSequenceValue(String sequenceValue) {
        this.sequenceValue = sequenceValue;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public int getSerialLength() {
        return serialLength;
    }

    public void setSerialLength(int serialLength) {
        this.serialLength = serialLength;
    }
}