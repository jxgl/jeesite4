/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.service;

import com.jeesite.common.entity.Page;
import com.jeesite.common.idgen.IdGenerate;
import com.jeesite.common.lang.DateUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.sys.dao.SmSequenceDao;
import com.jeesite.modules.sys.entity.SmSequence;
import org.junit.Assert;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static com.jeesite.common.lang.StringUtils.padPre;
import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * 编号自增表Service
 *
 * @author lib
 * @version 2018-04-11
 */
@Service
@Transactional(readOnly = true)
public class SmSequenceService extends CrudService<SmSequenceDao, SmSequence> {

    /**
     * 获取下一个序列，结果为日期：dateFormat+seqenceLength自增序列
     *
     * @param smSequence
     * @return
     */
    @Transactional(readOnly = false)
    public synchronized SmSequence getNext(SmSequence smSequence) {
        String tableName = smSequence.getTableName();
        Assert.assertTrue("tableName不能为空", StringUtils.isNotBlank(tableName));
        String columnName = smSequence.getColumnName();
        Assert.assertTrue("columnName不能为空", StringUtils.isNotBlank(columnName));
        String dateFormat = smSequence.getDateFormat();
        int seqenceLength = smSequence.getSerialLength();
        String prefix = smSequence.getPrefix();
        int preLength = StringUtils.isBlank(prefix) ? 0 : prefix.length();
        SmSequence selectSequence = this.dao.getByEntity(smSequence);
        String dateSeq = String.join(EMPTY, StringUtils.isBlank(prefix) ? EMPTY : prefix, DateUtils.formatDate(new Date(), dateFormat));
        if (selectSequence == null) {
            smSequence.setId(IdGenerate.nextId());
            smSequence.setSequenceValue(String.join(EMPTY, dateSeq, padPre("1", seqenceLength, '0')));
            this.dao.insert(smSequence);
            return smSequence;
        } else {
            String oldSeq = selectSequence.getSequenceValue();
            int preAndDftLength = preLength + dateFormat.length();
            String oldYearMonth = oldSeq.substring(0, preAndDftLength);
            if (oldYearMonth.equals(dateSeq)) {
                String val = oldSeq.substring(preAndDftLength).trim();
                selectSequence.setSequenceValue(String.join(EMPTY, dateSeq, padPre(String.valueOf(Integer.valueOf(val) + 1), seqenceLength, '0')));
            } else {
                selectSequence.setSequenceValue(String.join(EMPTY, dateSeq, padPre("1", seqenceLength, '0')));
            }
            this.dao.update(selectSequence);
            return selectSequence;
        }
    }

    /**
     * 获取单条数据
     *
     * @param smSequence
     * @return
     */
    @Override
    public SmSequence get(SmSequence smSequence) {
        return super.get(smSequence);
    }

    /**
     * 查询分页数据
     *
     * @param page       分页对象
     * @param smSequence
     * @return
     */
    @Override
    public Page<SmSequence> findPage(Page<SmSequence> page, SmSequence smSequence) {
        return super.findPage(page, smSequence);
    }

    /**
     * 保存数据（插入或更新）
     *
     * @param smSequence
     */
    @Override
    @Transactional(readOnly = false)
    public void save(SmSequence smSequence) {
        super.save(smSequence);
    }

    /**
     * 更新状态
     *
     * @param smSequence
     */
    @Override
    @Transactional(readOnly = false)
    public void updateStatus(SmSequence smSequence) {
        super.updateStatus(smSequence);
    }

    /**
     * 删除数据
     *
     * @param smSequence
     */
    @Override
    @Transactional(readOnly = false)
    public void delete(SmSequence smSequence) {
        super.delete(smSequence);
    }
}